package ru.tsc.panteleev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.panteleev.tm.api.repository.model.IProjectRepository;
import ru.tsc.panteleev.tm.api.service.model.IProjectService;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.panteleev.tm.exception.field.IdEmptyException;
import ru.tsc.panteleev.tm.exception.field.NameEmptyException;
import ru.tsc.panteleev.tm.exception.field.StatusIncorrectException;
import ru.tsc.panteleev.tm.exception.field.UserIdEmptyException;
import ru.tsc.panteleev.tm.model.Project;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Override
    @Transactional
    public Project create(@Nullable final String userId,
                          @Nullable final String name,
                          @Nullable final String description,
                          @Nullable final Date dateBegin,
                          @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setUser(getUserRepository().findById(userId));
        project.setName(name);
        project.setDescription(description);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        repository.add(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Project updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = findById(userId, id);
        project.setName(name);
        project.setDescription(description);
        repository.update(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Project changeStatusById(@Nullable final String userId,
                                    @Nullable String id,
                                    @Nullable Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @NotNull final Project project = findById(userId, id);
        project.setStatus(status);
        repository.update(project);
        return project;
    }

    @Override
    @Transactional
    public void set(@NotNull Collection<Project> projects) {
        repository.set(projects);
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull String userId) {
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull String userId, @Nullable Sort sort) {
        if (sort == null) return findAll(userId);
        return repository.findAllByUserIdSort(userId, sort);
    }

    @NotNull
    @Override
    public Project findById(@NotNull String userId, @NotNull String id) {
        Project project = repository.findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    @Transactional
    public void removeById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project;
        project = repository.findById(userId, id);
        if (project == null) return;
        repository.removeById(userId, id);
    }

    @Override
    @Transactional
    public void clear(@NotNull String userId) {
        repository.clearByUserId(userId);
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        return repository.existsById(userId, id);
    }

    @Override
    public long getSize(@NotNull String userId) {
        return repository.getSize(userId);
    }

}
