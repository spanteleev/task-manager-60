package ru.tsc.panteleev.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.panteleev.tm.model.AbstractUserOwnedModel;

@Repository
public class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

}
