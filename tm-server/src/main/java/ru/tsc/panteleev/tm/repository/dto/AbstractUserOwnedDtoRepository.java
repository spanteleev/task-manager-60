package ru.tsc.panteleev.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.tsc.panteleev.tm.dto.model.AbstractUserOwnedModelDto;

@Repository
public class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDto> extends AbstractDtoRepository<M>
        implements IUserOwnedDtoRepository<M> {

}
