package ru.tsc.panteleev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.panteleev.tm.dto.model.ProjectDto;
import ru.tsc.panteleev.tm.enumerated.Sort;

import java.util.Collection;
import java.util.List;

@Repository
public class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDto> implements IProjectDtoRepository {

    @Override
    public void set(@NotNull Collection<ProjectDto> projects) {
        clear();
        for (ProjectDto project : projects)
            add(project);
    }

    @NotNull
    @Override
    public List<ProjectDto> findAllByUserId(@NotNull String userId) {
        return entityManager.createQuery("FROM ProjectDto p WHERE p.userId = :userId", ProjectDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAllByUserIdSort(@Nullable String userId, @Nullable Sort sort) {
        if (sort == null) return findAllByUserId(userId);
        @NotNull final String query =
                String.format("SELECT p FROM ProjectDto p WHERE p.userId = :userId ORDER BY p.%s", getSortColumn(sort));
        return entityManager.createQuery(query, ProjectDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @NotNull List<ProjectDto> findAll() {
        return entityManager.createQuery("FROM ProjectDto", ProjectDto.class).getResultList();
    }

    @Nullable
    @Override
    public ProjectDto findById(@NotNull String userId, @NotNull String id) {
        List<ProjectDto> resultList =   entityManager
                .createQuery("SELECT p FROM ProjectDto p WHERE p.userId = :userId AND p.id = :id", ProjectDto.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultList();
        return getFirstRecord(resultList);
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        remove(findById(userId, id));
    }

    @Override
    public void clearByUserId(@NotNull String userId) {
        @NotNull final List<ProjectDto> projects = findAllByUserId(userId);
        for (ProjectDto project : projects)
            remove(project);
    }

    @Override
    public void clear() {
        @NotNull final List<ProjectDto> projects = findAll();
        for (ProjectDto project : projects)
            remove(project);
    }

    @Override
    public long getSize(@NotNull String userId) {
        return entityManager.createQuery("SELECT COUNT(p) FROM ProjectDto p", Long.class).getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        return findById(userId, id) != null;
    }

}
