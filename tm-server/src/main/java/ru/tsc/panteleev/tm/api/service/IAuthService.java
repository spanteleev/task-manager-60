package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.model.SessionDto;
import ru.tsc.panteleev.tm.dto.model.UserDto;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    SessionDto validateToken(@Nullable String token);

    @NotNull
    UserDto registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
