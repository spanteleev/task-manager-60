package ru.tsc.panteleev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.task.TaskBindToProjectRequest;
import ru.tsc.panteleev.tm.event.ConsoleEvent;
import ru.tsc.panteleev.tm.util.TerminalUtil;

@Component
public class TaskBindToProjectListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-bind-to-project";

    @NotNull
    public static final String DESCRIPTION = "Task bind to project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskBindToProjectListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        getProjectTaskEndpoint().bindToProjectTaskById(new TaskBindToProjectRequest(getToken(), projectId, taskId));
    }

}
