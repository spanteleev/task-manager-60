package ru.tsc.panteleev.tm.listener.data;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.panteleev.tm.listener.AbstractListener;
import ru.tsc.panteleev.tm.enumerated.Role;

@Getter
@Component
public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showDescription() {
        System.out.println("[" + getDescription().toUpperCase() + "]");
    }

}
